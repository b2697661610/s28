db.room.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description:"A simple room with all the basic necessities",
	rooms_available	: 10,
	isAvailable: "fales"
	
});


db.rooms.insertMany([
{

	name: "double",
	accomodates: 3,
	price: 2000,
	description:"A room fit for small family going on a vacation",
	rooms_available	: 15,
	isAvailable: "fales"
},

{

	name: "queen",
	accomodates: 4,
	price: 4000,
	description:"A room with a queen sized bed perfect for a simple getaway",
	rooms_available	: 15,
	isAvailable: "fales"
}]);

db.rooms.find({ name: "double"});




db.rooms.updateOne({name: "queen"}, {$set: { rooms_available	: 0 }});




db.rooms.deleteMany({rooms_available:0});